// @ts-check
const { test, expect } = require('@playwright/test');
import {Wait} from "../utils/wait";

test('Backend Scheduler check', async ({ page }) => {
  test.setTimeout(300000)
  await page.goto('/typo3');
  await page.fill('#t3-username', 'admin');
  await page.fill('#t3-password', 'Password.1');
  await page.waitForTimeout(500);
  await page.click('#t3-login-submit');
  await page.waitForLoadState('networkidle');
  await page.click('.modulemenu-name:is(:text("Scheduler"), :text("Scheduler"))')

  await Wait.nextXthMinute();

  let contentFrame = await page.frameLocator('#typo3-contentIframe')
  await contentFrame.locator('#moduleMenu').selectOption({label: 'Scheduler setup check'});
  await page.waitForLoadState('networkidle');
  await expect(contentFrame.locator('.callout.callout-success')).toBeVisible({timeout: 10000});
  await expect(contentFrame.locator('.callout.callout-info')).toHaveCount(2, {timeout: 10000});
});
