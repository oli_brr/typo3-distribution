<?php

namespace Site\Distribution\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Resource\Exception\InvalidFileException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FaviconMiddleware implements MiddlewareInterface
{
    protected ServerRequestInterface $request;

    /**
     * @throws InvalidFileException
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $this->request = $request;
        $this->addMetatagsToHead();

        return $handler->handle($this->request);
    }

    /**
     * @throws InvalidFileException
     */
    protected function addMetatagsToHead(): void
    {
        $headerData = '';
        $siteConfig = $this->getSiteSettings();
        $touch = $siteConfig['touch'];
        $png = $siteConfig['png'];
        $svg = $siteConfig['svg'];

        if ($touch) {
            $headerData .= '<link rel="apple-touch-icon" href="' . $touch . '" sizes="180x180">';
        }

        if ($png) {
            $headerData .= '<link rel="icon" href="' . $png . '" type="image/png" sizes="32x32">';
        }

        if ($svg) {
            $headerData .= '<link rel="icon" href="' . $svg . '" type="image/svg+xml">';
        }

        GeneralUtility::makeInstance(PageRenderer::class)->addHeaderData($headerData);
    }

    /**
     * @return array{png: ?string,svg: ?string,touch: ?string}
     */
    protected function getSiteSettings(): array
    {
        /** @var Site $site */
        $site = $this->request->getAttribute('site');
        $settings = $site->getSettings()->getAllFlat();

        return [
            'touch' => $settings['favicon.touch'] ?? null,
            'png' => $settings['favicon.png'] ?? null,
            'svg' => $settings['favicon.svg'] ?? null,
        ];
    }
}
